from distutils.core import setup

setup(
    name='kant_generator_pro',
    version='0.0.0',
    url='https://github.com/paul-nechifor/kant_generator_pro',
    packages=['kant_generator_pro']
)
